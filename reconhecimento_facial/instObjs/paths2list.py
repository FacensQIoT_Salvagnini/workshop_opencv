from os import listdir

def pathsIMG(dSet_Path):
    pathsList = []
    labelsList = []
    subSets = listdir(dSet_Path)
    for folder in subSets:
        filesPath = dSet_Path + "/" + folder
        files = listdir(filesPath)
        for imgFile in files:
            imgPath = filesPath + "/" + imgFile
            labelsList.append(folder)
            pathsList.append(imgPath)

    return pathsList, labelsList