import cv2
import numpy as np

class Face_Detector:
    def __init__(self, prototxt = None, model = None, confidence = 0.7):
        if prototxt and model is not None:
            self.confidence = confidence
            self.dNet = cv2.dnn.readNetFromCaffe(prototxt, model)

    def detects(self, img):
        self.fPositions = []
        self.fConfidences = []
        self.img = img
        (h, w) = self.img.shape[:2]
        blob = cv2.dnn.blobFromImage(cv2.resize(self.img, (300, 300)), 1.0, (300, 300), (104.0, 177.0, 123.0))
        self.dNet.setInput(blob)
        detections = self.dNet.forward()

        for i in range(0, detections.shape[2]):
            self.conf = detections[0, 0, i, 2]
            if self.conf > self.confidence:
                box = detections[0, 0, i, 3:7] * np.array([w, h, w, h])
                # fPosition = (xInicial, yInicial, xFinal, yFinal)
                self.fPositions.append(box.astype("int"))
                self.fConfidences.append(self.conf)
            
            if i == (detections.shape[2] - 1):
                return self.fPositions, self.fConfidences

    def drawFace(self):
        for i in range(0, len(self.fPositions)):
            (startX, startY, endX, endY) = self.fPositions[i]
            text = "{:.2f}%".format(self.fConfidences[i] * 100)
            yTxt = startY - 10 if startY - 10 > 10 else startY + 10
            cv2.rectangle(self.img, (startX, startY), (endX, endY), (0, 0, 255), 2)
            cv2.putText(self.img, text, (startX, yTxt), cv2.FONT_HERSHEY_SIMPLEX, 0.45, (0, 0, 255), 2)
