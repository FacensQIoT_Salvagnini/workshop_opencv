import cv2
from instObjs.paths2list import pathsIMG

# Carrega o extrator de 128-D Embeddings
vectors_128 = []
embExtractor = cv2.dnn.readNetFromTorch('modelo_recog/nn4.small2.v1.t7')

paths, labels = pathsIMG('dataset')

print(labels)
print('Quantidade de faces para treinamento = ', len(paths))

for path in paths:
    face = cv2.imread(path)
    faceBlob = cv2.dnn.blobFromImage(face,
                        1.0 / 255, (96, 96), (0, 0, 0), swapRB = True, crop = False)
    embExtractor.setInput(faceBlob)
    vectors_128.append(embExtractor.forward().flatten())

print('Quantidade de embeddings =', len(vectors_128))
print('Tamanho de cada embeddings = ', len(vectors_128[0]))

## Treinamento
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import GridSearchCV
from sklearn.preprocessing import LabelEncoder

print('-------------------Treinamento-------------------')
lb = LabelEncoder()
lbls = lb.fit_transform(labels)
print('Label original = ', labels)
# 1 - Crispim, 2 - Augusto, 3 - Stefany
print('Label transformada = ', lbls)

faceTrain, faceTest, lblsTrain, lblsTest = train_test_split(vectors_128, lbls, test_size=0.2, random_state=101, stratify = lbls)

# Importa o modelo de SVM
from sklearn.svm import SVC

model = SVC()
model = SVC(C = 10, gamma = 10, kernel = "linear", probability = True)
model.fit(faceTrain, lblsTrain)

predictions = model.predict(faceTest)
## Matriz de confusão
print("---------------- Matriz de confusao --------------------")
print(confusion_matrix(lblsTest, predictions))

''' CASO NECESSÁRIO BUSCA PELOS MELHOS PARÂMETROS PARA PARAMETRIZAR O MODELO'''
# # Busca por parâmetros mais otimizados
# param_grid = {"C":[0.001, 0.01, 0.1, 1, 10, 100, 1000], "gamma":[10, 1, 0.1, 0.01, 0.001, 0.0001], "kernel":["rbf", "linear"]}
# grid = GridSearchCV(SVC(), param_grid, verbose=1)
# grid.fit(faceTrain, lblsTrain)

# print(grid.best_params_)

# Salva o modelo para posterior utilização
last_model = SVC(C = 10, gamma = 10, kernel = "linear", probability = True)
last_model.fit(vectors_128, lbls)
from sklearn.externals import joblib
joblib.dump(model, 'modelo_finalizado.sav')