import cv2

# cv2.namedWindow("Nome_da_janela") criar uma janela para depois exibir a imagem
# Ao contrário do imshow, quando se cria uma janela anteriormente é possível definir se está será auto ajustável ou não
# Por padrão a flag é cv2.WINDOW_AUTOSIZE, porém setando cv2.WINDOW_NORMAL é possível redimensionar a imagem
cv2.namedWindow("Janela criada", cv2.WINDOW_NORMAL)

#cv2.imread("Diretório", modo de leitura)
# Diretório, pode ser o diretório relativo (Atual) ou o diretório total
# Modo de leitura: cv2.IMREAD_COLOR (1) / cv2.IMREAD_GRAYSCALE (0) / cv2.IMREAD_UNCHANGED (-1) -> Canal Alpha
imagem = cv2.imread("OPEN_CV.png", 0)
imagem_alpha = cv2.imread(r"C:\Users\felipe.salvagnini\Documents\MEGAsync\Cursos\Opencv\Trabalhando com imagens\alpha.png", cv2.IMREAD_UNCHANGED)

#cv2.imshow("Nome da janela", objeto_de_imagem) podem ser criadas diversas janelas, porém com nomes diferentes
cv2.imshow("Janela criada", imagem)
cv2.imshow("Imagem convencional", imagem)
cv2.imshow("Imagem com alpha", imagem_alpha)
# waitKey é uma função referente ao teclado, o argumento passado para essa é em ms
# A função espera o tempo enviado como argumento por uma tecla ser pressionada
# Caso seja passado 0, ela irá esperar indefinidamente
cv2.waitKey(0)
# Fecha todas as janelas criadas
# Para fechar uma janela especifica  cvw.destroyWindow("Nome_da_janela")
cv2.destroyAllWindows()

# Para salvar imagens se utiliza o cv2.imwrite(), sendo o primeiro argumento o nome do arquivo e o segundo o objeto
cv2.imwrite("opencv_gray.png", imagem)
cv2.imwrite("Alpha_img.png", imagem_alpha)
