import cv2

imagem = cv2.imread("OPEN_CV.png", 0)

cv2.imshow("Imagem convencional", imagem)

# Possível receber valores e utilizar para estruturas de decisão
# ESC em carácteres ASCII é 27
# While para sempre continuar exibindo a imagem mesmo que outra tecla seja pressionada (dif de ESC e s)
while True:
    # 0xFF é uma constante hexadecimal que representa 11111111 em binário, ao realizar a operação lógica AND
    # apenas os 8 bits iniciais de cv2.waitKey(0) serão mantidos, o 0xFF aplica uma máscara nos últimos bits
    # isso apenas para sistemas x64 bits
    key = cv2.waitKey(0) & 0xFF # 0xFF em sistemas x64 bit
    print(key)

    if key == 27:
        break

    # Dado uma string de tamanho 1, retorna um inteiro representando o ponto de código UNICODE do caráctere
    elif key == ord('s'):
        cv2.imwrite("Condicional.png", imagem)
        break

cv2.destroyAllWindows()
