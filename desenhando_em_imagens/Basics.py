# ---- FUNÇÕES ---- #
# cv2.line(), cv2.circle(), cv2.rectangle(), cv2.ellipse(), cv2.putText()
# Argumentos comuns:
# img: Objeto de imagen que será desenhado
# color: Cor da forma desenhada. BGR é uma tuple (255, 0, 0) => Azul. Para cinza, é passado apenas o valor escalar
# thickness: Espessura da corma desenhada. Caso -1 seja passado, se for uma forma fechada (Box, O), a figura será preenchida
# lineType: Tipos - linhas 8-connected, com anti-aliased. Padrão é linhas 8-connected, cv2.LINE_AA resulta em linhas
# com anti-aliasing, boas para curvas

import numpy as np
import cv2

# Criando uma imagem 512 x 512 totalmente preta
# img_l = np.zeros((512,512,3), np.uint8)
# img_r = np.zeros((512,512,3), np.uint8)
# img_c = np.zeros((512,512,3), np.uint8)
# img_el = np.zeros((512,512,3), np.uint8)
# img_p = np.zeros((512,512,3), np.uint8)
# img_T = np.zeros((512,512,3), np.uint8)
# print(img)
img_l = np.zeros((512,512,3), np.uint8)
img_r = img_l
img_c = img_l
img_el = img_l
img_p = img_l
img_T = img_l

# Desenhando uma linha (Passar as cordenadas de inicio e final da linha)
cv2.line(img_l, (10, 0), (10, 511), (0, 0, 255), 70) # Colocar 512 errado e perguntar o porque
cv2.imshow("Desenhando linha", img_l)

# Desenhando um retangulo (Coordenadas do canto superior esquerdo e do canto inferior direito)
cv2.rectangle(img_r, (384,0), (450, 300), (255, 0, 0), 2)
cv2.imshow("Desenhando retangulo", img_r)

# Desenhando um circulo (Coordenada do centro do circulo e seu raio)
cv2.circle(img_c, (255, 255), 100, (0, 255, 0), 5)
cv2.imshow("Desenhando circulo", img_c)

# Desenhando uma elipse (Coordenadas do centro, comprimento dos eixos (Maior e menor), angulo de rotação da ellipse -
# sentido anti-horário, startAngle e endAngle indica o final e o começo do arco eliptico na direção horária em -
# relação ao maior eixo (Valores 0 e 360 resultam em uma elipse total) )
# Mais informações procurar sobre cv2.ellipse
cv2.ellipse(img_el, (255, 255), (70, 10), 45, 50, 360, (255, 0, 0), 1)
cv2.imshow("Desenhando elipse", img_el)

# Desenhando um poligono (Coordenadas dos vertices, onde esses são transformados em um array de forma ROWSx1x2 -
# onde ROWS são os números de vertices e deve ser do tipo int32)
pontos = np.array([[20, 20], [60, 20], [80, 40], [80, 100], [60, 120], [20, 120]], np.int32)
pontos = pontos.reshape((-1, 1, 2))
cv2.polylines(img_p, [pontos], True, (255,0, 255), 2)
cv2.imshow("Desenhando poligono", img_p)
# Efetivo para criar várias linhas, criar uma lista com todas as linhas e posteriormente desenha-las com polylines

# Escrevendo textos (Dados de texto, coordenada da parte inferior esquerda de onde o texto será possicionado)
# Tipo da fonte (Fontes possíveis em cv2.putText)
# Escala da Fonte
# Para letras, é recomendado utilizar lineType = cv2.LINE_AA
font = cv2.FONT_HERSHEY_SIMPLEX
cv2.putText(img_T, "Felipe Crispim", (255, 255), font, 2, (255, 255, 255), 1, cv2.LINE_AA)
cv2.imshow("Escrevendo", img_T)
cv2.imshow("Fim", img_l)

cv2.waitKey(0)
