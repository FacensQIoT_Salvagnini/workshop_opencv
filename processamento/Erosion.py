# Operações comumente realizadas em imagens binárias
# Kernel percorre a imagem, e pixeis na imagem original, só serão considerados 1, se todos os
# pixeis sobre o kernel forem 1 também

import cv2
import numpy as np

img = cv2.imread("Base.png", 0)
kernel = np.ones((5,5), np.uint8)
erosion = cv2.erode(img, kernel, iterations = 1)

cv2.imshow("Normal", img)
cv2.imshow("Resultante", erosion)

cv2.waitKey(0)
cv2.destroyAllWindows()
