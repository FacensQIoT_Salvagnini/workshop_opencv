# Remove ruidos e bordas (São embaçadas através desta técnica)
# Há técnicas que mesmo embaçando, ainda mantém as bordas
# Filtro kernel, usar exemplo do video e da apostila

# Mediana

import cv2

img = cv2.imread("BIT.png")
blur = cv2.blur(img, (7, 7))
boxF = cv2.boxFilter(img, 32, (7, 7)) # Valor do meio mesmo esquema do filtro 2d, depth valor

cv2.imshow("Normal", img)
cv2.imshow("Blur", blur)
cv2.imshow("BoxF", boxF)

cv2.waitKey(0)
cv2.destroyAllWindows()
