import cv2

# Criar um objeto de captura de vídeo que irá referenciar o dispositivo de captura ou o diretório para um arquivo de video
# Nada mais do que um objeto que irá receber imagens e exibi-las, em uma taxa de quadros por segundo (fps)
# No caso é 0, da câmera do NOTEBOOK

cam = cv2.VideoCapture(0)
#print(cam.get(3))
cam.set(3, 1360)
cam.set(4, 768)
print(cam.get(3))
print(cam.get(4))

# --- LISTA DE PROPRIEDADES QUE PODEM SER ALTERADAS ---
# https://docs.opencv.org/2.4/modules/highgui/doc/reading_and_writing_images_and_video.html#videocapture-get

# cam.get(N) => Acessar as configurações da câmera, onde N é um número (0 - 18) representando uma propriedade do video
# Alguns valores podem ser modificados da seguintee forma cap.set(N, novo_valor)

while True:
    # Le o objeto de captura (quadro por quadro)
    # Read retorna um valor booleano (True = quadro lido corretamente / False = não lido)
    # Read retona também o frame lido
    # Caso seja retornado um erro que a captura do video não foi iniciada, valide com o metodo cam.isOpened()
    # Caso o método acima retorne True OK, caso contrário efetue a captura com cap.open()
    status, quadro = cam.read()
    if status is False:
        print("Dispositivo incorreto")
        break

    # Converte para escala de cinza
    cinza = cv2.cvtColor(quadro, cv2.COLOR_BGR2GRAY)

    # Exibi a imagem
    cv2.imshow("Quadro", cinza)
    if cv2.waitKey(1) & 0xFF == 27:
        break

cam.release()
cv2.destroyAllWindows()
