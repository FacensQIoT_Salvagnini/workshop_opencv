import cv2

cam = cv2.VideoCapture("Face Detection Demo.mp4")

while True:

    status, quadro = cam.read()

    if status == False:
        break
    else:
        cinza = cv2.cvtColor(quadro, cv2.COLOR_BGR2GRAY)
        cv2.imshow("Arquivo de video", cinza)
        if cv2.waitKey(20) & 0xFF == 27:
            break

cam.release()
cv2.destroyAllWindows()
